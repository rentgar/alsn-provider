//=============================================================================
// File: alsn_hud.gs
// Desc: Содержит реализацию для путевого HUD, отображающего состояние сигнала 
//       АЛС в рельсовой цепи и другие, сопутствующие данные
// Auth: Алексей 'Эрендир' Зверев 2019 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "interfaceprovider.gs"
include "ecl_bitlogic.gs"
include "alsn_engine.gs"
include "library.gs"
include "string.gs"

//=============================================================================
// Name: ALSN_Hud
// Desc: Содержит код для реализации HUD АЛСН
//       Класс не содержит внешнего интерфейса для взаимодействия и не 
//       предусматривает использования в составе какого либо кода
//=============================================================================
final class ALSN_Hud isclass Library
{

  //Набор флагов состояния
  define int STATE_LOCOFOUND                = 1 << 0;     //Локомотив найден
  define int STATE_AHEADSPEEDLIMIT          = 1 << 1;     //Впереди снижение скорости
  define int STATE_AHEADSPEEDUP             = 1 << 2;     //Впереди увеличение скорости
  define int STATE_UPHILL                   = 1 << 3;     //Подъём
  define int STATE_DOWNHILL                 = 1 << 4;     //Спуск
  define int STATE_FORWARDMOVING            = 1 << 5;     //Движение вперёд
  define int STATE_BACKWARDMOVING           = 1 << 6;     //Движение назад
  
  define int DIRECTION_NONE                 = 0;          //Не флаг. Направление не определено
  define int DIRECTION_FORWARD              = 1 << 0;     //Направление перед составом
  define int DIRECTION_BACKWARD             = 1 << 1;     //Направление за составом
  define int DIRECTION_RELATIVEDIRECTION    = 1 << 2;     //Поиск по направлению подвижной еденицы
                                                                                                                           
  Browser browser;                    //Браузер HUD
  StringTable strtable;               //Таблица строк для локаллизации
  Vehicle currvehicle;                //Текущая выбраная подвижная еденица
  Locomotive lstloco;                 //Последний контролируемый локомотив

  ALSN_Engine alsn;                   //Движок АЛСН

  int lststat = 0;                    //Последнее состояние: побитовая сумма флагов STATE_*
  int lstcode, lstfree;               //Последний код АЛСН и число свободных БУ
  
  string uisetfolder;                 //Папка, содержащая параметры интерфейса
  string uiforecolor;                 //Цвет текста в HUD
  string uiredcolor;                  //Цвет для красных надписей
  string uigreencolor;                //Цвет для зелёных надписей
  bool canuits17used      = false;    //Указатель, можно ли использовать интерфейс TRS2019
  int browserstyle;                   //Стиль для браузера HUD
  int browserwidth;                   //Ширина браузера
  int browserheight;                  //Высота браузера
  
  //Декларация основного потока
  thread void MainLooper(void);
	public final void CloseBrowser(void);
    
  //=============================================================================
  // Name: LoadBrowserUIProperties
  // Desc: Выполняет определение конфигурации интерфейса HUD
  //=============================================================================
  void LoadBrowserUIProperties(void)
  {
    float gameversion = TrainzScript.GetTrainzVersion();                                                                          //Определение текущей версии игры
    bool ts17uisupport = TrainzScript.DoesInstallationProvideRight(TrainzScriptBase.PRODUCTRIGHT_TS17UI) and canuits17used;       //Определение поддержки интерфейса TRS2019
    float currversion = 0.0;                                                                                                      //Здесь будет текущая версия, для конфига
    bool currts17ui = false;                                                                                                      //Есть ли поддержка у ьекущей конфигурации для TRS2019 интерфейса
    Soup uitablesoup = GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("ui-properties");                       //Таблица настроек интерфейса в конфиге
    int i, count = uitablesoup.CountTags();
    for(i = 0; i < count; ++i) {                                                                                                  //Перебираем все имеющиеся конфигурации
      Soup uisoup = uitablesoup.GetNamedSoup(uitablesoup.GetIndexedTagName(i));                                                   //Тут настройки интерфейса
      float version = uisoup.GetNamedTagAsFloat("version");                                                                       //Определение версии для которой выполнена конфигурация
      bool cants17ui = uisoup.GetNamedTagAsBool("TS17UI");                                                                        //Определение поддержки интерфейса ТРС2019 в конфигурации
      if(version <= gameversion and ((ts17uisupport and (version > currversion or (version == currversion and 
      !currts17ui and cants17ui))) or (!ts17uisupport and version > currversion and !cants17ui))) {                               //Проверяем подходимость конфигурации
        uisetfolder = uisoup.GetNamedTag("folder");
        uiforecolor = uisoup.GetNamedTag("forecolor");
        uiredcolor = uisoup.GetNamedTag("forecolor-red");
        uigreencolor = uisoup.GetNamedTag("forecolor-green");
        browserstyle = uisoup.GetNamedTagAsInt("style", Browser.STYLE_HUD_FRAME);
        browserwidth = uisoup.GetNamedTagAsInt("width");
        browserheight = uisoup.GetNamedTagAsInt("height");
        currversion = version;
        currts17ui = cants17ui;                 
      }
    }
  } 


  //=============================================================================
  // Name: GetVehicleSearchDirection
  // Desc: Определяет направление движение поезда
  // Parm: vehicle - Подвижная еденица, для состава которой нужно выполнить 
  //       проверку.
  // Parm: oldDirection - Побитовая сумма флагов DIRECTION_*, определяющая 
  //       направление движения при предыдущей проверке
  // Retn: Побитовая сумма флагов DIRECTION_*, определяющая направление поиска
  //=============================================================================
  int GetVehicleSearchDirection(Vehicle vehicle, int oldDirection)
  {
    bool relative = vehicle.GetDirectionRelativeToTrain();
    float vel = vehicle.GetVelocity(); 
    int ret = DIRECTION_NONE;
    if((vel > 0.0 and relative) or (vel < 0.0  and !relative)) ret = DIRECTION_FORWARD;
    else if(vel != 0.0) ret = DIRECTION_BACKWARD;
    if(((ret == DIRECTION_FORWARD or (ret == DIRECTION_NONE and oldDirection != DIRECTION_BACKWARD)) and relative) or 
    ((ret == DIRECTION_BACKWARD or (ret == DIRECTION_NONE and oldDirection == DIRECTION_BACKWARD)) and !relative)) ret = ret | DIRECTION_RELATIVEDIRECTION;
    return ret;
  }

  //=============================================================================
  // Name: UpdateHTML
  // Desc: Выполняет обновление браузера
  // Parm: code - Сумма флагов ALSN_Engine.IND_*, определяющих текущее показание
  //       АЛСН.
  // Parm: freecount - Колличество свободных блок-участков впереди (только при 
  //       приёме кода АЛСН-ЕН)
  // Parm: limit - Предстоящие ограничение скорости 
  // Parm: currlimit - Текущее ограничение скорости
  // Parm: grade - Текущий уклон пути (меньше 0 - спуск, больше 0 - подъём, равно
  //       0 - плоскость)
  //=============================================================================
  void UpdateHTML(int code, int freecount, float limit, float currlimit, float grade)
  {
    if(browser){
      if(code == (ALSN_Engine.IND_REDYELLOW | ALSN_Engine.IND_WHITE)) browser.SetParam(0, "<img width=29 height=106 src='hud/" + uisetfolder +"/als-redyellow-white.png' />");
      else if(code == ALSN_Engine.IND_WHITE) browser.SetParam(0, "<img width=29 height=106 src='hud/" + uisetfolder +"/als-white.png' />");
      else if(code == ALSN_Engine.IND_RED) browser.SetParam(0, "<img width=29 height=106 src='hud/" + uisetfolder +"/als-red.png' />");
      else if(code == ALSN_Engine.IND_REDYELLOW) browser.SetParam(0, "<img width=29 height=106 src='hud/" + uisetfolder +"/als-redyellow.png' />");
      else if(code == ALSN_Engine.IND_YELLOW) browser.SetParam(0, "<img width=29 height=106 src='hud/" + uisetfolder +"/als-yellow.png' />");
      else if(code == ALSN_Engine.IND_GREEN) browser.SetParam(0, "<img width=29 height=106 src='hud/" + uisetfolder +"/als-green.png' />");
      else browser.SetParam(0, "<img width=29 height=106 src='hud/" + uisetfolder +"/als-none.png' />");
      if(freecount >= 4) browser.SetParam(1, "<img src='hud/" + uisetfolder +"/span-four.png' />");
      else if(freecount == 3) browser.SetParam(1, "<img src='hud/" + uisetfolder +"/span-three.png' />");
      else if(freecount == 2) browser.SetParam(1, "<img src='hud/" + uisetfolder +"/span-two.png' />");
      else if(freecount == 1) browser.SetParam(1, "<img src='hud/" + uisetfolder +"/span-one.png' />");
      else browser.SetParam(1, "<img src='hud/" + uisetfolder +"/span-null.png' />");
      if(grade < 0.0) browser.SetParam(4, "<img src='hud/" + uisetfolder +"/gradedown.png' />");
      else if(grade > 0.0) browser.SetParam(4, "<img src='hud/" + uisetfolder +"/gradeup.png' />");
      else browser.SetParam(4, "<img src='hud/" + uisetfolder +"/gradenone.png' />");
      if(limit > 0.0 and limit < currlimit){
        browser.SetParam(2, "<img src='hud/" + uisetfolder +"/limitslow.png' />");
        browser.SetParam(3, uiredcolor);
      }else if(limit > currlimit){
        browser.SetParam(2, "<img src='hud/" + uisetfolder +"/limitfast.png' />");
        browser.SetParam(3, uigreencolor);
      }else{
        browser.SetParam(2, "<img src='hud/" + uisetfolder +"/limit.png' />");
        browser.SetParam(3, uiforecolor);
      }
      browser.LoadHTMLFile(me.GetAsset(), "hud/" + uisetfolder +"/browser.htm");
    }
	}

  //=============================================================================
  // Name: CreateBrowser
  // Desc: Выполняет создание браузера
  //=============================================================================
  public final void CreateBrowser(bool mustts17ui)
  {
    if(ECLLogic.XOR(mustts17ui, canuits17used) and browser) CloseBrowser();
    if(!browser){
      canuits17used = mustts17ui;
      LoadBrowserUIProperties();
      int width = Interface.GetDisplayWidth();
      browser = Constructors.NewBrowser();
      browser.SetCloseEnabled(false);
			browser.SetWindowStyle(browserstyle);
			browser.SetWindowRect(width - browserwidth, 100, width, browserheight + 100);
      me.MainLooper();
    }

    me.UpdateHTML(ALSN_Engine.IND_NONE, 0, 0.0, 0.0, 0.0);

    Interface.Print("Просто проверка");
    
    Library driverModule = TrainzScript.GetLibrary(me.GetAsset().LookupKUIDTable("driver-module"));
    GSObject[] objectParam = new GSObject[1];
		objectParam[0] = browser;
		driverModule.LibraryCall("add-hud", null, objectParam);
  }

  //=============================================================================
  // Name: CloseBrowser
  // Desc: Выполняет сокрытие браузера
  //=============================================================================
	public final void CloseBrowser(void)
	{
		if(browser){
		  Library driverModule = World.GetLibrary(me.GetAsset().LookupKUIDTable("driver-module"));
		  GSObject[] objectParam = new GSObject[1];
		  objectParam[0] = browser;
		  driverModule.LibraryCall("remove-hud", null, objectParam);
		  browser = null;
    }
	}

  //=============================================================================
  // Name: FindTracksides
  // Desc: Выполняет поиск путевых объектов по ходу движения
  // Parm: provider - 
  // Parm: direction - Побитовая сумма флагов  DIRECTION_*, определяющая 
  //       направление поиска
  // Retn: Массив растояний до объект:
  //       0  - Ограничение скорости
  //       1  - Растояние до ограничения скорости
  //       2  - Растояние до сигнальной точки
  //=============================================================================
  float[] FindTracksides(ALSN_Provider provider, int direction)
  {
    float[] ret = new float[3];
    Vehicle veh;
    int found;
    if(!provider) found = 2;
    if((direction & (DIRECTION_FORWARD | DIRECTION_BACKWARD)) != DIRECTION_BACKWARD) veh = currvehicle.GetMyTrain().GetVehicles()[0];
    else veh = currvehicle.GetMyTrain().GetVehicles()[currvehicle.GetMyTrain().GetVehicles().size() - 1];
    GSTrackSearch gs = veh.BeginTrackSearch(me.GetVehicleSearchDirection(veh, direction) & DIRECTION_RELATIVEDIRECTION);
    MapObject mo = gs.SearchNext();
    float distance = gs.GetDistance();
    while(mo and found < 3 and !mo.isclass(Vehicle)){
      Trackside ts = cast<Trackside>mo;
      if(provider and !(found & 2) and provider == mo){
        ret[2] = distance;
        found = found | 2;
      }
      if(ts and gs.GetFacingRelativeToSearchDirection() and !(found & 1)){
        float limit = ts.GetSpeedLimit();
        if(limit > 0.0 and limit != veh.GetMyTrain().GetSpeedLimit()){
          ret[0] = limit;
          ret[1] = distance;
          found = found | 1;
        }
      }
      mo = gs.SearchNext();
      distance = gs.GetDistance();
    }
    return ret;
  }

  //=============================================================================
  // Name: UpdateContent
  // Desc: Обновляет показания в HUD
  //=============================================================================
  void UpdateContent(void)
  {
    Locomotive trainloco; //Локомтив, который стоит во главе поезда
    if(currvehicle) trainloco = currvehicle.GetMyTrain().GetFrontmostLocomotive();
    if(trainloco){
      int direction = me.GetVehicleSearchDirection(trainloco, (lststat & (STATE_BACKWARDMOVING | STATE_FORWARDMOVING)) >> 5);
      if(!alsn or trainloco != lstloco){
        alsn = new ALSN_Engine();
        alsn.Init(trainloco);
        alsn.AllowRedYellowAndWhite = true;
        alsn.FrequencyDisabled = true;
        lstloco = trainloco;
      }
      if(direction & DIRECTION_RELATIVEDIRECTION and alsn.GetSearchDirection() != ALSN_Engine.SD_LOCOFORWARD) alsn.SetSearchDirection(ALSN_Engine.SD_LOCOFORWARD);
      else if(!(direction & DIRECTION_RELATIVEDIRECTION) and alsn.GetSearchDirection() != ALSN_Engine.SD_LOCOBACK) alsn.SetSearchDirection(ALSN_Engine.SD_LOCOBACK);
      int alsncode = alsn.SearchNextSignal(), freecount;
      ALSN_Provider provider = alsn.GetCurrentProvider();
      if(provider and alsncode & ALSN_Engine.IND_GREEN) freecount = provider.GetALSNFreeBlockSectionsCount();
      float[] tsinfo;
      if(direction & (DIRECTION_FORWARD | DIRECTION_BACKWARD)) tsinfo = me.FindTracksides(provider, direction);
      else tsinfo = me.FindTracksides(provider, (direction & DIRECTION_RELATIVEDIRECTION) | ((lststat & (STATE_BACKWARDMOVING | STATE_FORWARDMOVING)) >> 5));
      if(tsinfo[0] <= 0 and provider){
        tsinfo[0] = (cast<Trackside>((object)provider)).GetSpeedLimit();
        tsinfo[1] = tsinfo[2];
      }
      float velocity = trainloco.GetVelocity();
      float currlimit = trainloco.GetMyTrain().GetSpeedLimit();
      float currgrade = trainloco.GetTrackGradient();
      if(velocity < 0.0 or (!trainloco.GetDirectionRelativeToTrain() and velocity <= 0.0)) currgrade = currgrade * -1;
      
      if(!(lststat & STATE_LOCOFOUND) or ECLLogic.XOR(tsinfo[0] > 0.0 and tsinfo[0] < currlimit, lststat & STATE_AHEADSPEEDLIMIT) or ECLLogic.XOR(tsinfo[0] > currlimit, lststat & STATE_AHEADSPEEDUP) or
      ECLLogic.XOR(currgrade > 0.0, lststat & STATE_UPHILL) or ECLLogic.XOR(currgrade < 0.0, lststat & STATE_DOWNHILL) or alsncode != lstcode or freecount != lstfree)
        me.UpdateHTML(alsncode, freecount, tsinfo[0], currlimit, currgrade);

      if(provider){
        string sname = provider.GetALSNSignalName(), sfreq;
        string stname = provider.GetALSNStationName();
        int pfreq = provider.GetALSNFrequency();
        if(sname != "") browser.SetTrainzText("namesign", sname);
        else browser.SetTrainzText("namesign", "-- --");
        if(stname != "") browser.SetTrainzText("namestation", stname);
        else if(sname != "") browser.SetTrainzText("namestation", "");
        else browser.SetTrainzText("namestation", "-- --");
        if(tsinfo[2] >= 1000.0) browser.SetTrainzText("distancesign", strtable.GetString1("distance-full", String.FloatToPercentRoundString(tsinfo[2] / 1000.0)));
        else if(tsinfo[2] >= 0.0) browser.SetTrainzText("distancesign", strtable.GetString1("distance-low", (int)tsinfo[2]));
        else browser.SetTrainzText("distancesign", "-- --");
        if(alsncode & (ALSN_Engine.IND_GREEN | ALSN_Engine.IND_YELLOW | ALSN_Engine.IND_REDYELLOW) and !(alsncode & ALSN_Engine.IND_WHITE)){
          if(pfreq & ALSN_Provider.FREQ_ALSN25) sfreq = strtable.GetString("alsn25");
          else if(pfreq & ALSN_Provider.FREQ_ALSN50) sfreq = strtable.GetString("alsn50");
          else if(pfreq & ALSN_Provider.FREQ_ALSN75) sfreq = strtable.GetString("alsn75");
          if(pfreq & ALSN_Provider.FREQ_ALSNEN and sfreq != "") sfreq = sfreq + "+" + strtable.GetString("alsnens");
          else if(pfreq & ALSN_Provider.FREQ_ALSNEN) sfreq = strtable.GetString("alsnen");
          if(sfreq == "") browser.SetTrainzText("typesign", strtable.GetString("alsnnone"));
          else browser.SetTrainzText("typesign", sfreq);
        }else browser.SetTrainzText("typesign", strtable.GetString("alsnnone"));


        sname = null; stname = null; sfreq = null;
      }else{
        browser.SetTrainzText("namesign", "-- --");
        browser.SetTrainzText("namestation", "-- --");
        browser.SetTrainzText("distancesign", "-- --");
        browser.SetTrainzText("typesign", strtable.GetString("alsnnone"));
      }
      if((int)tsinfo[0] * 3.6 > 0.0){
        browser.SetTrainzText("limit", strtable.GetString1("limit", (int)(tsinfo[0] * 3.6)));
        if(tsinfo[1] >= 1000.0) browser.SetTrainzText("distancelimit", strtable.GetString1("distance-full", String.FloatToPercentRoundString(tsinfo[1] / 1000.0)));
        else browser.SetTrainzText("distancelimit", strtable.GetString1("distance-low", (int)tsinfo[1]));
      }else{
        browser.SetTrainzText("limit", "-- --");
        browser.SetTrainzText("distancelimit", "-- --");
      }
      if(currgrade < 0.0) browser.SetTrainzText("gradename", strtable.GetString("gtade-down"));
      else if(currgrade > 0.0) browser.SetTrainzText("gradename", strtable.GetString("gtade-up"));
      else browser.SetTrainzText("gradename", strtable.GetString("gtade-none"));
      if(currgrade != 0.0) browser.SetTrainzText("grade", strtable.GetString1("gtade", String.FloatToPercentRoundString(Math.Fabs(currgrade) * 10)));
      else browser.SetTrainzText("grade", "");
      browser.SetTrainzText("odo", strtable.GetString1("mileage-full", String.FloatToPercentRoundString(trainloco.GetOdometerDistance() / 1000)));
       
      if(alsncode & ALSN_Engine.IND_WHITE) browser.SetElementProperty("buttonskzh", "value", 1);
      else browser.SetElementProperty("buttonskzh", "value", 0);
      if(alsncode == ALSN_Engine.IND_RED) browser.SetElementProperty("buttonvk", "value", 1);
      else browser.SetElementProperty("buttonvk", "value", 0);
                 

      int olddir = lststat & (STATE_BACKWARDMOVING | STATE_FORWARDMOVING);
      lststat = STATE_LOCOFOUND;
      if(tsinfo[0] > 0.0 and tsinfo[0] < currlimit) lststat = lststat | STATE_AHEADSPEEDLIMIT;
      else if(tsinfo[0] > currlimit) lststat = lststat | STATE_AHEADSPEEDUP;
      if(currgrade > 0.0) lststat = lststat | STATE_UPHILL;
      else if(currgrade < 0.0) lststat = lststat | STATE_DOWNHILL;
      if(direction & DIRECTION_FORWARD or (!(direction & (DIRECTION_FORWARD | DIRECTION_BACKWARD)) and olddir & STATE_FORWARDMOVING)) lststat = lststat | STATE_FORWARDMOVING;
      else if(direction & DIRECTION_BACKWARD or (!(direction & (DIRECTION_FORWARD | DIRECTION_BACKWARD)) and olddir & DIRECTION_RELATIVEDIRECTION)) lststat = lststat | DIRECTION_RELATIVEDIRECTION;
      lstcode = alsncode;
      lstfree = freecount;
    }else{
      if(lststat != 0){me.UpdateHTML(ALSN_Engine.IND_NONE, 0, 0.0, 0.0, 0.0); lststat = 0;}
      browser.SetTrainzText("namesign", "-- --");
      browser.SetTrainzText("namestation", "-- --");
      browser.SetTrainzText("distancesign", "-- --");
      browser.SetTrainzText("typesign", "-- --");
      browser.SetTrainzText("limit", "-- --");
      browser.SetTrainzText("distancelimit", "-- --");
      browser.SetTrainzText("gradename", "-- --");
      browser.SetTrainzText("grade", "-- --");
      browser.SetTrainzText("odo", "-- --");
      lstloco = null; alsn = null;
    }
  }

  //=============================================================================
  // Name: MainLooper
  // Desc: Основной поток обработки
  //=============================================================================
  thread void MainLooper()
  {
    currvehicle = cast<Vehicle>World.GetCameraTarget();
    Message msg;
    me.PostMessage(me, "ALSN-HUD", "Update", 0.0);
    wait(){
      on "Camera", "Target-Changed", msg: { //Если камера была перемещена
        currvehicle = cast<Vehicle> msg.src;
        continue;
      }
      on "ALSN-HUD", "Update": { //Если переодическое обновление
        if(browser){
          me.UpdateContent();
          me.PostMessage(me, "ALSN-HUD", "Update", 1.0);
          continue;
        }else break;
      }
    }
  }

  //=============================================================================
  // Name: BrowserURLHandler
  // Desc: Обработчик кнопок в HUD
  //=============================================================================
  void BrowserURLHandler(Message msg)
  {
    if(browser and alsn and msg.src == browser){
      if(msg.minor == "live://alsn-hud/vk"){
        alsn.ResetToWhite();
        me.UpdateContent();
      }else if(msg.minor == "live://alsn-hud/skzh"){
        alsn.ResetRedYellow();
        me.UpdateContent();
      }
    }
  }

  //=============================================================================
  // Name: Init
  // Desc: Инициализирует библиотеку
  //=============================================================================
  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    strtable = myAsset.GetStringTable();
    me.AddHandler(me, "Browser-URL", "", "BrowserURLHandler");
  }

};
