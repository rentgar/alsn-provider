//=============================================================================
// File: alsn_provider.gs
// Desc: Содержит реализацию дешифратора АЛСН
// Auth: Алексей 'Эрендир' Зверев 2019 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "alsn_circuitrepeater.gs"
include "ecl_bitlogic.gs"

//=============================================================================
// Name: ALSN_State
// Desc: Вспомогательный класс для работы ALSN_Engine 
//=============================================================================
final class ALSN_State
{
  public ALSN_Provider lastchekprov;
  public ALSN_Provider lastprov;
  public ALSN_Provider lastfound;
  public ALSN_Provider lastsidingbrake; 
};


//=============================================================================
// Name: ALSN_Engine
// Desc: Реализует работу дешифратора АЛСН 
//=============================================================================
final class ALSN_Engine isclass GameObject
{

  //=============================================================================
  // Name: CODE_*
  // Desc: Каждая из констант определяет сигнал АЛСН, который был расшифрован из
  //       рельсовой цепи.
  // Note: Может быть побитовая сумма IND_REDYELLOW + IND_WHITE
  //=============================================================================
  public define int IND_NONE              = 0;                            //АЛСН отключён, или нет кода
  public define int IND_GREEN             = 1;                            //Зелёный сигнал АЛСН
  public define int IND_YELLOW            = 2;                            //Жёлтый сигнал АЛСН
  public define int IND_REDYELLOW         = 4;                            //Красно-жёлтый сигнал АЛСН
  public define int IND_RED               = 8;                            //Красный сигнал АЛСН
  public define int IND_WHITE             = 16;                           //Белый сигнал АЛСН
  public define int IND_REDYELLOWWHITE    = IND_REDYELLOW | IND_WHITE;    //Красно-жёлтый + белый сигнал АЛСН (только при УКБМ)   

  //=============================================================================
  // Name: FREQ_*
  // Desc: Частота фильтра АЛСН. 
  // Note: Допускается побитовая сумма FREQ_ALSN25 + FREQ_ALSN75
  //=============================================================================
  public define int FREQ_ALSN25           = 1;                            //АЛСН25
  public define int FREQ_ALSN50           = 2;                            //АЛСН50
  public define int FREQ_ALSN75           = 4;                            //АЛСН75
  public define int FREQ_ALSN2575         = FREQ_ALSN25 | FREQ_ALSN75;    //АЛСН25 + АЛСН75

  //=============================================================================
  // Name: SD_*
  // Desc: Направление поиска от локомотива 
  //=============================================================================
  public define int SD_TRAINDIRECTION     = 0;                            //Поиск производится в направлении поезда
  public define int SD_LOCOFORWARD        = 1;                            //Вперёд по направлению локомотива
  public define int SD_LOCOBACK           = 2;                            //Назад относительно направления локомотива

  //=============================================================================
  // Name: Init
  // Desc: Инициализирует движок АЛСН
  // Parm: sloco - Объект Locomotive, с которым будет работать движок АЛСН
  //=============================================================================
  public final void Init(Locomotive sloco);

  //=============================================================================
  // Name: Init
  // Desc: Инициализирует движок АЛСН
  // Parm: sloco - Объект Locomotive, с которым будет работать движок АЛСН
  // Parm: code - Одна из констант CODE_*, определяющая начальное показание АЛСН
  //       Если значение свойства AllowRedYellowAndWhite задано как true, то 
  //       допускается побитовая сумма IND_REDYELLOW + IND_WHITE
  //=============================================================================
  public final void Init(Locomotive sloco, int code); 

  //=============================================================================
  // Name: GetLocomotive
  // Desc: Возвращает локомотив, с которым связан текущий движок АЛСН
  // Retn: Объект Locomotive, представляющий локомтив, с которым саязан движок 
  //       АЛСН, или null, если инициализация не выполнена
  //=============================================================================
  public final Locomotive GetLocomotive(void); 
  
  //=============================================================================
  // Name: GetFrequency
  // Desc: Получает частоту фильтра АЛСН
  // Retn: Значение одной из констант FREQ_*. Допускается побитовая сумма
  //       FREQ_ALSN25 + FREQ_ALSN75 
  //=============================================================================
  public final int GetFrequency(void); 
  
  //=============================================================================
  // Name: GetSearchDirection
  // Desc: Получает направление поиска от локомтива
  // Retn: Значение одной из констант SD_*
  //=============================================================================
  public final int GetSearchDirection(void); 
  
  //=============================================================================
  // Name: GetCurrentCode
  // Desc: Возвращает текущий сигнал АЛСН
  // Retn: Значение одной из констант CODE_*. Если значение свойства
  //       AllowRedYellowAndWhite задано true, то допускается побитовая сумма
  //       IND_REDYELLOW + IND_WHITE
  //=============================================================================
  public final int GetCurrentCode(void); 

  //=============================================================================
  // Name: SetFrequency
  // Desc: Задаёт частоту фильтра АЛСН
  // Parm: freq - Значение одной из констант FREQ_*. Допускается побитовая сумма
  //       FREQ_ALSN25 + FREQ_ALSN75 
  //=============================================================================
  public final void SetFrequency(int freq); 

  //=============================================================================
  // Name: SetSearchDirection
  // Desc: Задаёт направление поиска от локомтива
  // Parm: dir - Значение одной из констант SD_*
  //=============================================================================
  public final void SetSearchDirection(int dir); 

  //=============================================================================
  // Name: ResetToWhite
  // Desc: Сбрасывает текущий красный сигнал АЛСН на белый
  //=============================================================================
  public final void ResetToWhite(void);

  //=============================================================================
  // Name: SearchNextSignal
  // Desc: Выполняет поиск сигнальной точки и обновление состояния АЛСН. 
  //       Возвращает новое показание АЛСН
  // Retn: Значение одной из констант CODE_*. Если значение свойства
  //       AllowRedYellowAndWhite задано true, то допускается побитовая сумма
  //       IND_REDYELLOW + IND_WHITE
  //=============================================================================
  public final int SearchNextSignal(void); 

  //=============================================================================
  // Name: ResetToWhite
  // Desc: Сбрасывает КЖ при сигнале КЖ+Б, или устанавливает КЖ+Б при сигнале Б  
  // Note: Только если свойство AllowRedYellowAndWhite имеет значение true
  //=============================================================================
  public final void ResetRedYellow(void); 

  //=============================================================================
  // Name: ResetToNullState
  // Desc: Выполняет полный сброс АЛСН к начальному состоянию 
  //=============================================================================
  public final void ResetToNullState(void); 

  //=============================================================================
  // Name: GetCurrentProvider
  // Desc: Возвращает текущую сигнальную точку, на основе которой составлено 
  //       показание АЛСН
  // Retn: Объект ALSN_Provider, представляющий сигнальную точку на основе 
  //       которой выолнены текущие показания АЛСН. Или null, если сигнальная
  //       точка не найдена (потеря сигнала)
  //=============================================================================
  public final ALSN_Provider GetCurrentProvider(void);

    //Устаревшие
//  public final bool IsOnlyWhiteIndicator(void); //Возвращает указаель, включён ли режим отображения только белого
//  public final void SetOnlyWhiteIndicator(bool white); //Устанавливает режим отображения только белого

  //=============================================================================
  // Name: AllowRedYellowAndWhite
  // Desc: Свойство, указывающее, может ли быть сигнал АЛСН КЖ+Б (для УКБМ):
  //       значение true, если сигнал КЖ+Б допустим, в противном случае - 
  //       значение false 
  //=============================================================================
  public bool AllowRedYellowAndWhite = false; 

  //=============================================================================
  // Name: AllowRedYellowAndWhite
  // Desc: Свойство, указывающее, что нужно отключить проверку частоты 
  //       кодирования АЛСН сигнала.Ззначение true, если нужно игнорировать
  //       частоту кодирвоания АЛСН, иначав противном случае - значение false
  // Note: Частота ALSN_Provider.FREQ_ALSNEN не учитывается 
  //=============================================================================
  public bool FrequencyDisabled = false; 

	//
	// РЕАЛИЗАЦИЯ
	//

  Locomotive loco; //Тут будет локомотив, к которому прикреплен АЛСН
  int cfreq = FREQ_ALSN50, sdir = SD_TRAINDIRECTION, cind = IND_NONE;
  int innerstate = 0; //Внутренее состояние: 1 - Пропустить один поиск светофора; 2 - Отображать только белый; 4 - локомотив направлен по ходу вижения
  float firstAxleOffset = 0.0; //Смещение первой колёсной пары от ноля

  ALSN_State lststate = new ALSN_State(); //Последний проследованый провайдер
  
  define int SIGNALSTYPES = ALSN_Provider.TYPE_IN | ALSN_Provider.TYPE_ROUTER | ALSN_Provider.TYPE_OUT | ALSN_Provider.TYPE_DIVIDE | ALSN_Provider.TYPE_PASSING | ALSN_Provider.TYPE_CIRCUIT;
  define int STATIONSIGNALTYPES = ALSN_Provider.TYPE_IN | ALSN_Provider.TYPE_ROUTER | ALSN_Provider.TYPE_OUT | ALSN_Provider.TYPE_DIVIDE;
 
  //Обновление смещения оси первой колёсной пары
  final void UpdateFirstAxelOffset(void)
  {
    Soup prop = loco.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("alsn-engine");
    if ((sdir == SD_TRAINDIRECTION and loco.GetDirectionRelativeToTrain()) or sdir == SD_LOCOFORWARD) firstAxleOffset = prop.GetNamedTagAsFloat("frontaxleoffset", 0.0);
    else firstAxleOffset = prop.GetNamedTagAsFloat("rearaxleoffset", 0.0);
    if (firstAxleOffset < 0.0) firstAxleOffset = 0.0;
  }


  //Поиск следующего светофора (-1 - сброс работы, 0 - Сигнал не найден или отсутствует кодирование участка, 1 - КЖ, 2 - жёлтый, 3 - зелёный
  final int SearchSignal(void){
    if (!loco) return -1;
    bool hasbraker = false;
    bool hasjunction = false;
    bool codedev = false;
    bool codedevhas = false;
    GSTrackSearch searcher = loco.BeginTrackSearch((sdir == SD_TRAINDIRECTION and loco.GetDirectionRelativeToTrain()) or sdir == SD_LOCOFORWARD); //Начинаем поиск от локомотива
    MapObject foundobject = searcher.SearchNext();
    float distance = searcher.GetDistance();
    while (foundobject and distance < Math.Rand(4500.0, 5000.0)) { //Пока найден какой-нибудь путевой объект и расстояние до него не более 5 км (от 4,5 до 5 км возможны помехи)
      if (distance >= firstAxleOffset) {  //Игнорируем всё, что до первой колёсной пары
        ALSN_Provider provider = cast<ALSN_Provider>((object)foundobject);  //Пытаемся получить объект интерфейса АЛСН
        ALSN_CircuitRepeater rc = cast<ALSN_CircuitRepeater>foundobject;    //Пытаемся получить специальный повторитель
        if (provider and (!rc or (rc and searcher.GetFacingRelativeToSearchDirection() and rc.Enabled()))) { //Если удалось найти провайдер АЛСН
          if (rc) provider = rc.ControlProvider();        //Если найден повторитель, получаем из него основной провайдер, который он повторяет
          int type = provider.GetALSNTypeSignal();        //Получаем тип провайдера
          bool facing = searcher.GetFacingRelativeToSearchDirection(); //Определяем, лицом ли к нам стоит провайдер
          if (type & SIGNALSTYPES or rc) {                //Проверяем, является ли тип учавствующим в рельсовых цепях
            int siding;                                   //Тут будет хранится информация о кодировании съездов
            if (!rc) { //Если не повторитель РЦ
              siding = provider.GetALSNSiding();
              if (!facing and type & STATIONSIGNALTYPES /*and type & ALSN_Provider.TYPE_IN*/ and !codedevhas) { //Если провайдер входной светофор, тыльной стороной, и не встречался ранее, то
                codedev = siding & ALSN_Provider.SIDING_TO;
                codedevhas = true;
              }
            } else siding = rc.GetALSNSiding() & ALSN_Provider.SIDING_TO;
            if (facing and (!lststate.lastchekprov or lststate.lastchekprov != provider)) { //Если провайдер не соответствует тому, что встречался ранее
              lststate.lastprov = lststate.lastchekprov;  //Перемещаем последний известный провайдер, в последний проследованый
              lststate.lastchekprov = provider;           //Запоминамем текущий провайдер
              lststate.lastfound = provider;
              lststate.lastsidingbrake = null;
            } else if (facing and lststate.lastchekprov == provider and !lststate.lastfound) lststate.lastfound = provider;
            if (facing) {                                 //Если провайдер стоит лицом
              int alsncode, freq;                         //Тут будут код РЦ и частота кодирования
              if (rc) {                                   //Если повторитель
                alsncode = rc.GetALSNCode();              //Получаем код сигнала
                freq = rc.GetALSNFrequency();             //Получение частоты кодирования сигнала
              } else {
                alsncode = provider.GetALSNCode();        //Получаем код сигнала
                freq = provider.GetALSNFrequency();       //Получение частоты кодирования сигнала
              }
              int ltype = ALSN_Provider.TYPE_NONE, lsiding = ALSN_Provider.SIDING_NONE;
              if (lststate.lastprov) {
                ltype = lststate.lastprov.GetALSNTypeSignal();
                lsiding = lststate.lastprov.GetALSNSiding();
              }
              bool lscodedev = !(ltype & SIGNALSTYPES) or (ltype & STATIONSIGNALTYPES /*ALSN_Provider.TYPE_IN */and lsiding & ALSN_Provider.SIDING_FROM) or (!(ltype & STATIONSIGNALTYPES/* ALSN_Provider.TYPE_IN*/) and lsiding & ALSN_Provider.SIDING_TO);
              bool ccodedev = (type & STATIONSIGNALTYPES/* ALSN_Provider.TYPE_IN */and siding & ALSN_Provider.SIDING_FROM) or (!(type & STATIONSIGNALTYPES/*ALSN_Provider.TYPE_IN*/) and siding & ALSN_Provider.SIDING_TO);
              bool hascode = ((FrequencyDisabled and freq & 15) or freq & cfreq) and ((!hasjunction and !hasbraker) or ((codedevhas and codedev and lscodedev) or (!codedevhas and ccodedev and lscodedev)));
              if (hascode) return alsncode & 3;
              else return 0;
            }
          }
          if (!rc and !facing and type & ALSN_Provider.TYPE_SIDINGBRAKE) {
            if (!lststate.lastsidingbrake or lststate.lastsidingbrake == provider) {
              lststate.lastsidingbrake = provider;
              hasbraker = true;
            }
          }
        } else if (foundobject.isclass(Junction)) {
          lststate.lastsidingbrake = null;
          hasjunction = true;
          hasbraker = false;
        } else if (foundobject.isclass(Vehicle)) {
          lststate.lastfound = null; 
          return 0;                                     //Если найден ПС, возвращаем код, что светофор не найден
        } 
      }
      foundobject = searcher.SearchNext();
      distance = searcher.GetDistance();
    }
    lststate.lastfound = null;
    return 0;
  }

  final bool ReupdateLastProviderForward(void)
  {
    GSTrackSearch gs = loco.BeginTrackSearch((sdir == SD_TRAINDIRECTION and loco.GetDirectionRelativeToTrain()) or sdir == SD_LOCOFORWARD); //Начинаем поиск от локомотива
    MapObject mo = gs.SearchNext();
    float distance = gs.GetDistance();
    while(mo and distance < firstAxleOffset){
      ALSN_Provider provider = cast<ALSN_Provider>((object)mo); //Пытаемся получить объект интерфейса АЛСН
      if(provider and gs.GetFacingRelativeToSearchDirection() and provider.GetALSNTypeSignal() & 63){ //Если светофор расположен к нам лицом, то
        lststate.lastchekprov = provider;
        lststate.lastprov = null;
        lststate.lastfound = null;
        return true;
      }
      mo = gs.SearchNext();
      distance = gs.GetDistance();
    }
    return false;
  }

  //Поиск и обновление последнего пройденого провайдера
  final void ReupdateLastProvider(void)
  {
    if(firstAxleOffset > 0.0 and me.ReupdateLastProviderForward()) return; //Если есть смещение первой оси, то ищем сначала светофор впереди
    GSTrackSearch gs = loco.BeginTrackSearch(!((sdir == SD_TRAINDIRECTION and loco.GetDirectionRelativeToTrain()) or sdir == SD_LOCOFORWARD)); //Начинаем поиск от локомотива
    MapObject mo = gs.SearchNext();
    while(mo){
      ALSN_Provider provider = cast<ALSN_Provider>((object)mo); //Пытаемся получить объект интерфейса АЛСН
      if(provider and !gs.GetFacingRelativeToSearchDirection() and provider.GetALSNTypeSignal() & 63){ //Если светофор расположен к нам спиной, то
        lststate.lastchekprov = provider;
        lststate.lastprov = null;
        lststate.lastfound = null;
        return;
      }
      mo = gs.SearchNext();
    }
    lststate.lastchekprov = null;
    lststate.lastprov = null;
    lststate.lastfound = null;
  }


//Получение локомотива, к которому подключён АЛСН
  public final Locomotive GetLocomotive(void) 
  {
    return loco;
  } 
  
//Получение текущей частоты кодирования сигнала  
  public final int GetFrequency(void) 
  {
    return cfreq;
  }

  //Получение текущего направления поиска светофора   
  public final int GetSearchDirection(void)
  {
    return sdir;
  }
   
  //Возвращает указаель, включён ли режим отображения только белого
  public obsolete final bool IsOnlyWhiteIndicator(void)
  {
    Interface.WarnObsolete("Function bool ALSN_Engine.IsOnlyWhiteIndicator(void) is obsolete. Do not use it");
    return (bool)(innerstate & 2);
  }    

  //Получение текущего провайдера, для которого отображаются показания  
  public final ALSN_Provider GetCurrentProvider(void)
  {
    return lststate.lastfound;
  }

  //Получение текщего сигнала АЛСН
  public final int GetCurrentCode(void)
  {
    if (!loco) { Exception("ALSN Engine is not initialized"); return 0; }
    if (innerstate & 2) return IND_WHITE;
    return cind;
  }

  //Установка частоты кодирования сигнала
  public final void SetFrequency(int freq)
  {
    if (!loco) { Exception("ALSN Engine is not initialized"); return; }
    if(freq != FREQ_ALSN25 and freq != FREQ_ALSN50 and
    freq != FREQ_ALSN75 and freq != FREQ_ALSN2575) { Exception("Function 'ALSN_Engine.SetFrequency()' has wrong parametr"); return; }
    cfreq = freq;
  }

  //Установка направления поиска светофоров
  public final void SetSearchDirection(int dir)
  {
    if (!loco) { Exception("ALSN Engine is not initialized"); return; }
    if(dir != SD_TRAINDIRECTION and dir != SD_LOCOFORWARD and dir != SD_LOCOBACK) {
      Exception("Function 'ALSN_Engine.SetSearchDirection()' has wrong parametr");
      return;
    }
    if(dir != sdir){ //Если направление было изменено
      bool nforward = (dir == SD_TRAINDIRECTION and loco.GetDirectionRelativeToTrain()) or dir == SD_LOCOFORWARD;
      bool oforward = (sdir == SD_TRAINDIRECTION and loco.GetDirectionRelativeToTrain()) or sdir == SD_LOCOFORWARD;
      sdir = dir;
      if (ECLLogic.XOR(nforward, oforward)) {
        UpdateFirstAxelOffset();
        ReupdateLastProvider();
      }
    }
  }

  //Устанавливает режим отображения только белого
  public obsolete final void SetOnlyWhiteIndicator(bool white)
  {
    Interface.WarnObsolete("Function bool ALSN_Engine.IsOnlyWhiteIndicator(void) is obsolete. Do not use it");
    if (!loco) { Exception("ALSN Engine is not initialized"); return; }
    if(ECLLogic.XOR(white, innerstate & 2)){
      innerstate = ECLLogic.SetBitMask(innerstate, 2, white);
      if (!white) {
        ReupdateLastProvider();
        cind = IND_WHITE;
      }
    }
  }

  //Сброс КЖ
  public final void ResetRedYellow(void)
  {
    if (!loco) { Exception("ALSN Engine is not initialized"); return; }
    if (AllowRedYellowAndWhite and ECLLogic.GetBitMask(cind, IND_REDYELLOW | IND_WHITE)) cind = IND_WHITE;
    else if (AllowRedYellowAndWhite and cind == IND_WHITE) cind = IND_REDYELLOW | IND_WHITE;
  }

  //Сброс К на Б
  public final void ResetToWhite(void)
  {
    if (!loco) { Exception("ALSN Engine is not initialized"); return; }
    if (cind = IND_RED) cind = IND_WHITE;
  }

  //Поиск следующего светофора
  public final int SearchNextSignal(void)
  {
    if (!loco) { Exception("ALSN Engine is not initialized"); return 0; }
    if (innerstate & 2) return IND_WHITE;
    if (innerstate & 1) {
      innerstate = ECLLogic.SetBitMask(innerstate, 1, false);
      ReupdateLastProvider();
      return GetCurrentCode();
    }
    if (ECLLogic.XOR(loco.GetDirectionRelativeToTrain(), innerstate & 4)) { //Если направление состава изменилось
      innerstate = ECLLogic.SetBitMask(innerstate, 4, loco.GetDirectionRelativeToTrain());
      if (sdir == SD_TRAINDIRECTION) {
        UpdateFirstAxelOffset();
        ReupdateLastProvider();
      }
    }
    int result = SearchSignal(); //Поиск кода
    if (result == 3) cind = IND_GREEN; //Если сигнал З, то сохраняем код для З
    else if (result == 2) cind = IND_YELLOW; //Если сигнал Ж, то сохраняем код Ж
    else if (result == 1) cind = IND_REDYELLOW; //Если сигнал КЖ, то сохраняем код КЖ
    else if (result == 0 and cind == IND_YELLOW and AllowRedYellowAndWhite) cind = IND_REDYELLOW | IND_WHITE; //Если сигнал отсутствует, а последний код Ж и есть возможность сигнала КЖ+Б, то сохраняем код КЖ+Б
    else if (result == 0 and cind == IND_REDYELLOW) cind = IND_RED; //Если сигнал отсутствует и последний код КЖ, то сохраняем код К.
    else if (result == 0 and (cind == IND_GREEN or cind == IND_YELLOW or cind == IND_NONE)) cind = IND_WHITE; //Если сигнал отсутствует и имеетс разрешающий код или АЛСН было отключено, то сохраняем код Б
    return cind;
  }

  public final void ResetToNullState(void)
  {
    if (!loco) { Exception("ALSN Engine is not initialized"); return; }
    innerstate = ECLLogic.SetBitMask(0, 4, loco.GetDirectionRelativeToTrain());
    UpdateFirstAxelOffset();
    ReupdateLastProvider();
    cind = IND_NONE;
  }

  //Инициализация
  public final void Init(Locomotive sloco)
  {
    if (!sloco) { Exception("Function 'ALSN_Engine.Init()' has parametr 'sloco' is null"); return; }
    if (loco) { Exception("Already initialized"); return; }
    loco = sloco;
    ResetToNullState();
  }

   //Инициализация
  public final void Init(Locomotive sloco, int code)
  {
    if(code != IND_NONE and code != IND_GREEN and code != IND_YELLOW and code != IND_REDYELLOW and code != IND_RED and code != IND_WHITE and code != (IND_REDYELLOW | IND_WHITE)) {
      Exception("Function 'ALSN_Engine.Init()' has parametr 'code' is wrong");
      return;
    }
    Init(sloco);
    cind = code;
  }

};
