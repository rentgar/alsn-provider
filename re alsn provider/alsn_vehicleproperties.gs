//=============================================================================
// File: alsn_provider.gs
// Desc: Содержит классы для получение специальных характеристик подвежных
//       единиц, необходимых для работы АЛСН.
// Auth: Алексей 'Эрендир' Зверев 2023 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "vehicle.gs"

//=============================================================================
// Name: ALSN_VehiclePropertiesInterface
// Desc: Класс интерфейс для характеристик подвижной единицы.
// Note: Требуется наследование в классе подвижной единицы (вагона или 
//       локомотива) (Vehicle или Locomotice), например:
//       
//       class MyVehicle isclass Vehicle, ALSN_VehiclePropertiesInterface
//       {
//          ...
//       }
//
//       или
//       
//       class MyLocomotive isclass Locomotive, ALSN_VehiclePropertiesInterface
//       {
//          ...
//       }
//
//       Для работы требуется взывать функцию VehiclePropertiesInterfaceInit() 
//       в Init(Asset) класса наследника. Допускается своя реализация 
//       интерфейса через переопределение функций интерфейса. 
//=============================================================================
class ALSN_VehiclePropertiesInterface
{

  //=============================================================================
  // Name: AxleCount
  // Desc: Возвращает количество осей у подвижной единицы.
  // Retn: Значение больше или равное 2, определяющее количество осей для этой
  //       подвижной единицы.
  // None: Требуется предварительный однократный вызов 
  //       VehiclePropertiesInterfaceInit() в функции Init(Asset) класса 
  //       наследника, иначе будет возникать исключение.
  //       Допускается реализация через переоперделение в классе наследнике. 
  //=============================================================================
  public int AxleCount();
  
  //=============================================================================
  // Name: FrontFirstAxleOffset
  // Desc: Возвращает смещение первой колёсной пары с головы подвижной единицы.
  // Retn: Значение больше ноля (0) в метрах, определяющее смещение от нулевой
  //       точки подвижной единицы до первой колёсной пары, расположенной с её
  //       головы.
  // None: Требуется предварительный однократный вызов 
  //       VehiclePropertiesInterfaceInit() в функции Init(Asset) класса 
  //       наследника, иначе будет возникать исключение.
  //       Допускается реализация через переоперделение в классе наследнике. 
  //=============================================================================
  public float FrontFirstAxleOffset();
  
  //=============================================================================
  // Name: RearFirstAxleOffset
  // Desc: Возвращает смещение первой колёсной пары с тыла подвижной единицы.
  // Retn: Значение больше ноля (0) в метрах, определяющее смещение от нулевой
  //       точки подвижной единицы до первой колёсной пары, расположенной с её
  //       тыла.
  // None: Требуется предварительный однократный вызов 
  //       VehiclePropertiesInterfaceInit() в функции Init(Asset) класса 
  //       наследника, иначе будет возникать исключение.
  //       Допускается реализация через переоперделение в классе наследнике. 
  //=============================================================================
  public float RearFirstAxleOffset();
  
  //=============================================================================
  // Name: VehiclePropertiesInterfaceInit
  // Desc: Инициализирует характеристики подвижной единицы для реализации по
  //       умолчанию.
  // None: Требуется однократно вызвать в функции Init(Asset) класса наследника, 
  //       после вызова inherited().
  //       Не требуется вызывать, если интерфейс полностью реализуется в классе
  //       наследнике.
  //=============================================================================
  final void VehiclePropertiesInterfaceInit();
  
	//
	// РЕАЛИЗАЦИЯ
	//
  
  Vehicle _myveh;
  int _axlecount;
  float _frontaxleoffset;
  float _rearaxleoffset;
  
  public int AxleCount() 
  {                   
    if (!_myveh) { Interface.Exception("ALSN_VehiclePropertiesInterface.AxleCount> Функция VehiclePropertiesInterfaceInit() не была предварительно вызвана."); return 0; }
    return _axlecount;
  }
  
  public float FrontFirstAxleOffset()
  {
    if (!_myveh) { Interface.Exception("ALSN_VehiclePropertiesInterface.FrontFirstAxleOffset> Функция VehiclePropertiesInterfaceInit() не была предварительно вызвана."); return 0.0; }
    return  _frontaxleoffset;
  }

  public float RearFirstAxleOffset()
  {
    if (!_myveh) { Interface.Exception("ALSN_VehiclePropertiesInterface.RearFirstAxleOffset> Функция VehiclePropertiesInterfaceInit() не была предварительно вызвана."); return 0.0; }
    return  _rearaxleoffset;
  }

  final void VehiclePropertiesInterfaceInit()
  {
    if (_myveh) { Interface.Exception("ALSN_VehiclePropertiesInterface.VehiclePropertiesInterfaceInit> Функция VehiclePropertiesInterfaceInit() уже была вызвана ранее."); return; }
    _myveh = cast<Vehicle>((object)me);
    if (!_myveh) { Interface.Exception("ALSN_VehiclePropertiesInterface.VehiclePropertiesInterfaceInit> Интерфейс ALSN_VehiclePropertiesInterface может быть унаследован только наследником класса Vehicle."); return; }
    Asset asset = _myveh.GetAsset();
    if (!_myveh) { Interface.Exception("ALSN_VehiclePropertiesInterface.VehiclePropertiesInterfaceInit> Функция VehiclePropertiesInterfaceInit() должна быть вызвана в функции Init(Asset) класса наследника."); return; }
    Soup soup = asset.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("alsn-engine");
    _frontaxleoffset = soup.GetNamedTagAsFloat("frontaxleoffset", -1.0);
    _rearaxleoffset = soup.GetNamedTagAsFloat("rearaxleoffset", -1.0);
    _axlecount = soup.GetNamedTagAsInt("axlecount", 4);
    if (_frontaxleoffset < 0.0) _frontaxleoffset = _myveh.GetLength() * 0.45;
    if (_rearaxleoffset < 0.0) _rearaxleoffset = _myveh.GetLength() * 0.45;
    if (_axlecount < 2) _axlecount = 4;
  }

};

//=============================================================================
// Name: ALSN_VehicleProperties
// Desc: Статический класс, предоставляющий инструмены получения характеристик
//       подвижных единиц.
//=============================================================================
static class ALSN_VehicleProperties
{

  //=============================================================================
  // Name: PullAxleCount
  // Desc: Извлекает количество осей для указанной подвижной единицы.
  // Parm: vehicle - объект Vehicle, представляющий подвижную единицу для которой
  //       необходимо извлечь количество осей. 
  // Retn: Значение больше или равное 2, определяющее количество осей для 
  //       указанной подвижной единицы.
  //=============================================================================
  public int PullAxleCount(Vehicle vehicle);
  
  //=============================================================================
  // Name: PullFrontFirstAxleOffset
  // Desc: Извлекает смещение первой колёсной пары с головы подвижной единицы.
  // Parm: vehicle - объект Vehicle, представляющий подвижную единицу для которой
  //       необходимо извлечь смещение первой колёсной пары. 
  // Retn: Значение больше ноля (0) в метрах, определяющее смещение от нулевой
  //       точки подвижной единицы до первой колёсной пары, расположенной с её
  //       головы.
  //=============================================================================
  public float PullFrontFirstAxleOffset(Vehicle vehicle);
  
  //=============================================================================
  // Name: PullRearFirstAxleOffset
  // Desc: Извлекает смещение первой колёсной пары с тыла подвижной единицы.
  // Parm: vehicle - объект Vehicle, представляющий подвижную единицу для которой
  //       необходимо извлечь смещение первой колёсной пары. 
  // Retn: Значение больше ноля (0) в метрах, определяющее смещение от нулевой
  //       точки подвижной единицы до первой колёсной пары, расположенной с её
  //       тыла.
  //=============================================================================
  public float PullRearFirstAxleOffset(Vehicle vehicle);

  //=============================================================================
  // Name: GetFirstAxleOffset
  // Desc: Получает смещение первой колёсной пары с указанной стороны подвижной
  //       единицы.
  // Parm: vehicle - объект Vehicle, представляющий подвижную единицу для которой
  //       необходимо извлечь смещение первой колёсной пары. 
  // Parm: direct - направление, в котором нужно получить смещение первой
  //       колёсной пары, где значение true, если нужно получить смещение с 
  //       головы подвижной единицы; в противном случае — значение false. 
  // Retn: Значение больше ноля (0) в метрах, определяющее смещение от нулевой
  //       точки подвижной единицы до первой колёсной пары.
  //=============================================================================
  public float GetFirstAxleOffset(Vehicle vehicle, bool direct);

	//
	// РЕАЛИЗАЦИЯ
	//

  public int PullAxleCount(Vehicle vehicle)
  {
    if (!vehicle) { Interface.Exception("ALSN_VehicleProperties.PullAxleCount> Значение аргумента vehicle не может быть null."); return 0; }
    ALSN_VehiclePropertiesInterface interface = cast<ALSN_VehiclePropertiesInterface>((object)vehicle);
    int axlecount = 0;
    if (!interface) {
      Soup soup = vehicle.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("alsn-engine");
      axlecount = soup.GetNamedTagAsInt("axlecount", 4);
    } else axlecount = interface.AxleCount();
    if (axlecount < 2) axlecount = 4; 
    return axlecount;
  }
  
  public float PullFrontFirstAxleOffset(Vehicle vehicle)
  {
    if (!vehicle) { Interface.Exception("ALSN_VehicleProperties.PullFrontFirstAxleOffset> Значение аргумента vehicle не может быть null."); return 0.0; }
    ALSN_VehiclePropertiesInterface interface = cast<ALSN_VehiclePropertiesInterface>((object)vehicle);
    float offset = 0;
    if (!interface) {
      Soup soup = vehicle.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("alsn-engine");
      offset = soup.GetNamedTagAsFloat("frontaxleoffset", -1.0);
    } else offset = interface.FrontFirstAxleOffset();
    if (offset < 0.0) offset = vehicle.GetLength() * 0.45;
    return offset;
  }
   
  public float PullRearFirstAxleOffset(Vehicle vehicle)
  {
    if (!vehicle) { Interface.Exception("ALSN_VehicleProperties.PullRearFirstAxleOffset> Значение аргумента vehicle не может быть null."); return 0.0; }
    ALSN_VehiclePropertiesInterface interface = cast<ALSN_VehiclePropertiesInterface>((object)vehicle);
    float offset = 0;
    if (!interface) {
      Soup soup = vehicle.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("alsn-engine");
      offset = soup.GetNamedTagAsFloat("rearaxleoffset", -1.0);
    } else offset = interface.RearFirstAxleOffset();
    if (offset < 0.0) offset = vehicle.GetLength() * 0.45;
    return offset;
  }

  public float GetFirstAxleOffset(Vehicle vehicle, bool direct)
  {
    if (!vehicle) { Interface.Exception("ALSN_VehicleProperties.GetFirstAxleOffset> Значение аргумента vehicle не может быть null."); return 0.0; }
    if (direct) return PullFrontFirstAxleOffset(vehicle);
    return PullRearFirstAxleOffset(vehicle);
  }

};