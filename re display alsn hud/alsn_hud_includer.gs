//=============================================================================
// File: alsn_hud_includer.gs
// Desc: Содержит реализацию правила, для включения или отключения отображения
//       HUD АЛСН в машинисте
// Auth: Алексей 'Эрендир' Зверев 2019 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "scenariobehavior.gs"
include "ecl_bitlogic.gs"
include "alsn_hud.gs"


//=============================================================================
// Name: ALSN_Hud_Includer
// Desc: Реализация правила для управления отображением АЛСН HUD  в машинисте.
//=============================================================================
final class ALSN_Hud_Includer isclass ScenarioBehavior
{
  StringTable strtable;                           //Таблица строк
  ALSN_Hud hudlibrary;                            //Ссылка на провайдер АЛСН  
  bool hudvisible                 = true;         //Указатель, определяющий, нужно ли отображать HUD
  bool ts17uisupport;                             //Указатель, определяющий, поддерживате ли игра интерфейс TRS2019
  bool mustts12ui                 = false;        //Указатель, что нужно использовать интерфейс TRS2019 для HUD
  
  //=============================================================================
  // Name: CheckTS17UISupport
  // Desc: Определяет доступен ли интерфейс TRS2019
  // Retn: Значение true, если интерфейс TRS2019 доступен, в противном случае -
  //       значение false
  //=============================================================================
  bool CheckTS17UISupport(void)
  {
    float gameversion = TrainzScript.GetTrainzVersion();                                                                 //Определение текущей версии игры
    bool ts17uisupport = TrainzScript.DoesInstallationProvideRight(TrainzScriptBase.PRODUCTRIGHT_TS17UI);                //Определение поддержки интерфейса TRS2019
    if(!ts17uisupport) return false;                                                                                     //Если в игре нет поддержки интерфейса TRS2019, то просто возвращаем false
    float currversion = 0.0;                                                                                             //Здесь будет текущая версия, для конфига
    bool currts17ui = false;                                                                                             //Есть ли поддержка у ьекущей конфигурации для TRS2019 интерфейса
    Soup uitablesoup = hudlibrary.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("ui-properties");   //Таблица настроек интерфейса в конфиге
    int i, count = uitablesoup.CountTags();
    for(i = 0; i < count; ++i) {                                                                                         //Перебираем все имеющиеся конфигурации
      Soup uisoup = uitablesoup.GetNamedSoup(uitablesoup.GetIndexedTagName(i));                                          //Тут настройки интерфейса
      float version = uisoup.GetNamedTagAsFloat("version");                                                              //Определение версии для которой выполнена конфигурация
      bool cants17ui = uisoup.GetNamedTagAsBool("TS17UI");                                                               //Определение поддержки интерфейса ТРС2019 в конфигурации
      if(version <= gameversion and ((ts17uisupport and (version > currversion or (version == currversion and 
      !currts17ui and cants17ui))) or (!ts17uisupport and version > currversion and !cants17ui))) {                      //Проверяем подходимость конфигурации
        currversion = version;
        currts17ui = cants17ui;                 
      }
    }
    return currts17ui; 
  }
  
	string GetPropertyType(string propertyId)
	{
		if(propertyId == "visibled" or propertyId == "ts17ui") return "link";
    return inherited(propertyId);
	}
  
	public string GetDescriptionHTML(void)
	{
    string ret = "<font size=8><b>" + strtable.GetString("title") + "</b></font><br><br>";
    string vsisiblestate, ts17uistate;
    if(hudvisible) vsisiblestate = strtable.GetString("yes");
    else vsisiblestate = strtable.GetString("no");
    if(mustts12ui) ts17uistate = strtable.GetString("yes");
    else ts17uistate = strtable.GetString("no");
    ret = ret + HTMLWindow.StartTable();
    ret = ret + HTMLWindow.StartRow() + HTMLWindow.MakeCell(strtable.GetString("canvisible") + ":") + HTMLWindow.StartCell() + 
                HTMLWindow.MakeLink("live://property/visibled", vsisiblestate, strtable.GetString("tooltip-visible")) + HTMLWindow.EndCell() + HTMLWindow.EndRow();
    if(ts17uisupport and hudvisible) {
      ret = ret + HTMLWindow.StartRow() + HTMLWindow.MakeCell(strtable.GetString("ts17ui") + ":") + HTMLWindow.StartCell() + 
                  HTMLWindow.MakeLink("live://property/ts17ui", ts17uistate, strtable.GetString("tooltip-ts17ui")) + HTMLWindow.EndCell() + HTMLWindow.EndRow();
    }  
    ret = ret + HTMLWindow.EndTable() + "<br />";
    if(ts17uisupport) {
      ret = ret + strtable.GetString("ts17ui-description");
      if(mustts12ui) ret = ret + "<br /><br /><font color=#f38b76><b>Внимание!</b></font>Интерфейс HUD адаптирован под интерфейс Trainz 2019 не полностью. Шрифты могут отображатся не так, как ожидается.";
    }
    return ret;
  }
  
	void LinkPropertyValue(string propertyId)
	{
		if(propertyId == "visibled") {
      hudvisible = !hudvisible;
      if(!hudvisible) mustts12ui = false;
    }else if(propertyId == "ts17ui" and ts17uisupport and hudvisible) mustts12ui = !mustts12ui;
    else inherited(propertyId);
	}
  
  public string GetChildRelationshipIcon(ScenarioBehavior child){ return "none"; }

	public void Pause(bool paused)
	{
    if(ECLLogic.XOR(paused, me.IsPaused())){
		  me.SetStateFlags(PAUSED, paused);
      if(!paused and !me.IsComplete()){
        if(hudvisible) hudlibrary.CreateBrowser(mustts12ui);
        else hudlibrary.CloseBrowser();
        me.SetComplete(true);
      }
    }
  }

	public Soup GetProperties(void)
	{
		Soup soup = inherited();
		soup.SetNamedTag("Visible", hudvisible);
		soup.SetNamedTag("TS17UI", ts17uisupport and hudvisible and mustts12ui);
		return soup;
	}

	public void SetProperties(Soup soup)
	{
		inherited(soup);
		hudvisible = soup.GetNamedTagAsBool("Visible", hudvisible);
		mustts12ui = soup.GetNamedTagAsBool("TS17UI", mustts12ui) and ts17uisupport and hudvisible;
    me.Reset();
	}

  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    strtable = myAsset.GetStringTable();
		hudlibrary = cast<ALSN_Hud>World.GetLibrary(myAsset.LookupKUIDTable("provider-hud"));
    ts17uisupport = CheckTS17UISupport();
    me.SetStateFlags(DOES_COMPLETE, true);
  }

};
