# Универсальный провайдер АЛСН #
Базовая библиотека для реализации кодирования рельсовых цепей АЛСН в Trainz Rail Road Simulator. В состав входил АЛСН HUD.

### Последняя версия ###
Текущая версия библиотеки 1.5

Изменения:

* Код адаптирован под Trainz a New Era и Trainz 2019.
* Отказ от поддержки Trainz 2012.
* Добавлен дизайн ALSN Hud под TANE и TRS2019, с автоматическим выбором.
* В тестовом варианте добавлена поддержка интерфейса TRS17UI (В Trainz 2019. Включается в настройках правила Display ALSN HUD).
* Добавлены дополнительные константы в ALSN Provider.
* Изменены требования к кодированию сигнала.
 
Полный список изменений [здесь][changelog]

## Подключение библиотеки ##
Система распространяется в виде библиотеки скриптов подключаемой к реализующему проекту. Для подключения библиотеки необходимо в конфигурационном файле (config.txt) реализующего проекта указать ссылку на библиотеку (kuid2:151055:60049:3) в блоке script-include-table. 

Например: 
```
script-include-table 
{
    alsnproviderlibrary                          <kuid2:151055:60049:3> 
} 
```
**Внимание! Рекомендуемо указывать в номере KUID последнюю актуальную версию библиотеки.** 

Подробно об API библиотеки на [wiki][api]

## Состав ##

* ALSN Provider (<kuid2:151055:60049:3>) - Основная библиотека, содержащая базовые классы и API для реализации АЛСН в Trainz.
* Display ALSn HUD (<kuid2:151055:60050:3>) - Правило для управления отображением АЛСН HUD в режиме машиниста.


[changelog]: <https://bitbucket.org/rentgar/alsn-provider/src/master/CHANGELOG.md> "Список изменений"
[api]: <https://bitbucket.org/rentgar/alsn-provider/wiki/Home> "API провайдера АЛСН"
